package test;

import API.IEdge;
import API.ILista;
import API.SistemaRecomendacion;
import VOS.VOGeneroPelicula;
import VOS.VOPeliculaPlan;
import VOS.VOTeatro;
import VOS.VOUsuario;
import junit.framework.TestCase;

public class PruebaSistemaRecomendacion extends TestCase {
	public final static String RUTA_PELIS = "data/Proyecto 3/movies_filtered.json";
	public final static String RUTA_RAITING = "data/Proyecto 3/ratings_filtered.csv";
	public final static String RUTA_CARTELERA1 = "data/Proyecto 3/Programacion/dia1.json";
	public final static String RUTA_CARTELERA2= "data/Proyecto 3/Programacion/dia2.json";
	public final static String RUTA_CARTELERA3 = "data/Proyecto 3/Programacion/dia3.json";
	public final static String RUTA_CARTELERA4 = "data/Proyecto 3/Programacion/dia4.json";
	public final static String RUTA_CARTELERA5 = "data/Proyecto 3/Programacion/dia5.json";
	public final static String RUTA_TEAROS = "data/Proyecto 3/teatros_v4.json";
	public final static String RUTA_SIMILITUDES = "data/Proyecto 3/parteA/simMatriz.json";
	public final static String RUTA_RED = "data/Proyecto 3/tiempos1.json";


	private SistemaRecomendacion sr;

	private void setupEscenario1()
	{
		sr = new SistemaRecomendacion();
	}

	public void test()
	{
		setupEscenario1();
		assertTrue(sr.cargarPeliculas(RUTA_PELIS));
		assertTrue(sr.cargarRatingsSR(RUTA_RAITING));
		assertTrue(sr.cargarTeatros(RUTA_TEAROS));
		assertTrue(sr.cargarRed(RUTA_RED));
		assertTrue(sr.cargarSimilitudes(RUTA_SIMILITUDES));
		assertTrue(sr.cargarCartelera(RUTA_CARTELERA1,RUTA_CARTELERA2, RUTA_CARTELERA3, RUTA_CARTELERA4, RUTA_CARTELERA5 ));


		ILista <IEdge<VOTeatro>> listaMapa = sr.generarMapa();
		//for (IEdge<VOTeatro> edge : listaMapa)
			//System.out.println(edge.toString());
		System.out.println("Arcos MST:" + listaMapa.darNumeroElementos() + " - NumTeatros:" + sr.sizeTeatros());
		assertEquals(sr.sizeTeatros()-1, listaMapa.darNumeroElementos());;

		int numTeatrosRuta = 4;
		ILista <ILista<VOTeatro>> listaRutas = sr.rutasPosible(listaMapa.verPrimero().getNodoLlegada(), numTeatrosRuta);
		for (ILista<VOTeatro> listaTeatros : listaRutas){
			assertEquals(numTeatrosRuta, listaTeatros.darNumeroElementos());
			String recorrido = "";
			for (VOTeatro teatro : listaTeatros)
				recorrido += " -->" + teatro.getNombre();
			//System.out.println(recorrido);
		}
		
//		for (VOPeliculaPlan peliPlan : sr.PlanPeliculas(sr.darUsuarioCualquiera(), 1))
//			System.out.println(peliPlan.toString());
		
		VOUsuario usu = sr.darUsuarios().darValor(577);
		ILista<VOPeliculaPlan> lista = sr.PlanPeliculas(usu, 1);
		VOGeneroPelicula genero = new VOGeneroPelicula("Action");
		
		ILista<VOPeliculaPlan> listados = sr.PlanPorGenero(genero, usu);
		
		
	}
}
