package API;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;







import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import VOS.VOUsuarioPelicula;

import com.csvreader.CsvReader;

import VOS.VOFranquicia;
import VOS.VOGeneroPelicula;
import VOS.VOPelicula;
import VOS.VOPeliculaPlan;
import VOS.VOTeatro;
import VOS.VOUbicacion;
import VOS.VOUsuario;
import data_structures.Edge;
import data_structures.EncadenamientoSeparadoTH;
import data_structures.ListaEnlazada;
import data_structures.ListaEnlazadaSimple;
import data_structures.MaxHeapCP;
import data_structures.Nodo;
import data_structures.WeightedGraph;
import VOS.VORating;
import VOS.VORecorrido;

public class SistemaRecomendacion implements ISistemaRecomendacion {
	private WeightedGraph <VOTeatro, VOTeatro> grafo;
	private EncadenamientoSeparadoTH <Integer, VOPelicula> tablaPeliculas;
	private EncadenamientoSeparadoTH <Integer, VOUsuario> tablaUsuarios;
	private EncadenamientoSeparadoTH <String, VOTeatro> tablaTeatros;
	private Time ultimaFuncion;

	public SistemaRecomendacion (){
		grafo =  new WeightedGraph <VOTeatro, VOTeatro> (100); 
		tablaPeliculas = new EncadenamientoSeparadoTH<> (100);
		tablaUsuarios = new EncadenamientoSeparadoTH<> (100);
		tablaTeatros = new EncadenamientoSeparadoTH<>(100);
	}
	@Override
	public ISistemaRecomendacion crearSR() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean cargarTeatros(String ruta) {
		// TODO Auto-generated method stub
		BufferedReader br;
		try {
			br = new BufferedReader (new FileReader (new File (ruta)));
			String lista = br.readLine();
			String actual = null;
			while (true){
				actual = br.readLine();
				if (actual == null)
					break;
				lista = lista + actual;
			}
			lista = lista.substring(3, lista.length());
			JSONArray array = new JSONArray(lista);
			for (Object obj : array){
				JSONObject object =  (JSONObject) obj;
				String nombre = object.getString("Nombre");
				String ubicacionLatLong = object.getString("UbicacionGeografica(Lat|Long)");
				String [] arregloLatLong = {ubicacionLatLong.substring(0, ubicacionLatLong.indexOf("|")),   ubicacionLatLong.substring(ubicacionLatLong.indexOf("|")+1)};
				VOUbicacion ubicacion = null;
				try{
					double lat = Float.parseFloat(arregloLatLong[0].trim());
					double longi = Float.parseFloat(arregloLatLong[1].trim());
					ubicacion = new VOUbicacion(lat, longi);
				}
				catch (NumberFormatException e){
					e.printStackTrace();
				}
				String franquicia = object.getString("Franquicia");
				VOFranquicia voFanquicia = new VOFranquicia(franquicia);
				VOTeatro teatro = new VOTeatro(nombre, ubicacion, voFanquicia);
				grafo.agregarVertice(teatro, teatro);
				tablaTeatros.insertar(nombre, teatro);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}


	public boolean cargarCartelera (String ruta){
		return true;
	}
	public boolean cargarCartelera(String ruta1, String ruta2,String ruta3, String ruta4,String ruta5) {
		Time ultimaFuncion = null;
		Time ultimaFuncion2 = null;
		String [] arregloRutas = {ruta1, ruta2, ruta3, ruta4, ruta5};
		int indice = 1;
		SimpleDateFormat format = new SimpleDateFormat("HH:mm");
		BufferedReader br;
		try {
			for (String ruta : arregloRutas){
				br = new BufferedReader (new FileReader (new File (ruta)));
				String lista = br.readLine();
				String actual = null;
				while (true){
					actual = br.readLine();
					if (actual == null)
						break;
					lista = lista + actual;	 
				}
				JSONArray array = new JSONArray(lista);
				for (Object obj : array){
					JSONObject object =  (JSONObject) obj;
					String teatroNombre = object.getString("Teatro");
					JSONArray peliculas = object.getJSONArray("Pelicula");
					for (Object objPeli : peliculas){
						JSONObject pelicula =  (JSONObject) objPeli;
						int id = pelicula.getInt("Id");
						JSONArray funciones = pelicula.getJSONArray("Funciones");
						for (Object objFuncion : funciones){
							JSONObject funcion =  (JSONObject) objFuncion;
							String horaString = funcion.getString("Hora");
							VOPelicula voPelicula = tablaPeliculas.darValor(id);
							VOTeatro voTeatro = tablaTeatros.darValor(teatroNombre);
							try {
								Time timeInicio = new Time(format.parse(horaString).getTime());
								int hora = (Integer.parseInt(horaString.substring(0, horaString.indexOf(":"))))+2;
								horaString = hora + horaString.substring(horaString.indexOf(":"));
								Time timeFin = new Time(format.parse(horaString).getTime());
								VOPeliculaPlan peliPlan = new VOPeliculaPlan(voPelicula, voTeatro, timeInicio, timeFin, indice);
								voPelicula.agregarFuncion(peliPlan);
								voTeatro.agregarFuncion(peliPlan);
								if (ultimaFuncion == null || ultimaFuncion.getTime() < timeFin.getTime()){
									ultimaFuncion = timeFin;
									ultimaFuncion2 = timeInicio;
								}
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}
					}
				}
				br.close();
				indice++;
			} 
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		System.out.println("�ltima Funci�n:" + ultimaFuncion + "&&" +ultimaFuncion2);
		this.ultimaFuncion = ultimaFuncion2;
		return true;
	}

	@Override
	public boolean cargarRed(String ruta) {
		// TODO Auto-generated method stub
		BufferedReader br;
		try {
			br = new BufferedReader (new FileReader (new File (ruta)));
			String lista = br.readLine();
			String actual = null;
			while (true){
				actual = br.readLine();
				if (actual == null)
					break;
				lista = lista + actual;	 
			}
			JSONArray array = new JSONArray(lista);
			for (Object obj : array)
			{
				JSONObject object =  (JSONObject) obj;
				String teatro1 = object.getString("Teatro 1").trim();
				String teatro2 = object.getString("Teatro 2").trim();
				double minutos = object.getDouble("Tiempo (minutos)");
				grafo.agregarArco(tablaTeatros.darValor(teatro1), tablaTeatros.darValor(teatro2), minutos);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean cargarPeliculas (String ruta){
		BufferedReader br;
		try {
			br = new BufferedReader (new FileReader (new File (ruta)));
			String lista = br.readLine();
			String actual = null;
			while (true){
				actual = br.readLine();
				if (actual == null)
					break;
				lista = lista + actual;

			}
			JSONArray array = new JSONArray(lista);
			for (Object obj : array)
			{
				JSONObject object =  (JSONObject) obj;
				int id = object.getInt("movie_id");
				String nombre = object.getString("title");
				String generosString = object.getString("genres");
				String [] generosArreglo = new String [1];
				if (!generosString.contains("|"))
					generosArreglo[0] = generosString.trim();
				else{
					char [] charArreglo = generosString.toCharArray();
					ListaEnlazadaSimple<String> listaGeneros = new ListaEnlazadaSimple<>();
					String gene = "";
					for (char c : charArreglo){
						if (c == '|'){
							listaGeneros.agregarElementoFinal(gene.trim());
							gene = "";
							continue;
						}
						gene += c;
					}
					generosArreglo = new String [listaGeneros.darNumeroElementos()];
					int i = 0;
					for (String s : listaGeneros){
						generosArreglo[i] = s;
						i++;
					}
				}
				VOPelicula peli = new VOPelicula(id, nombre, generosArreglo);
				tablaPeliculas.insertar(id, peli);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean cargarRatingsSR(String rutaRaitings) {
		try {
			CsvReader peliculas_import = new CsvReader(rutaRaitings);
			peliculas_import.readHeaders();

			while (peliculas_import.readRecord())
			{
				int idUsuario = Integer.parseInt(peliculas_import.get(0));
				int idPelicula = Integer.parseInt(peliculas_import.get(1));
				double ratingDouble = Double.parseDouble(peliculas_import.get(2));
				VORating rating = new VORating (idUsuario, idPelicula,ratingDouble);

				VOPelicula pelicula = tablaPeliculas.darValor(idPelicula);
				pelicula.agregarRating(rating);
				VOUsuario usuario = tablaUsuarios.darValor(idUsuario);
				if (usuario == null){
					usuario = new VOUsuario(idUsuario);
					tablaUsuarios.insertar(idUsuario, usuario);
				}
				usuario.agregarRating(rating, pelicula.getTitulo());	
			}
			peliculas_import.close();

		} catch (FileNotFoundException e) {
			return false;
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	public boolean cargarSimilitudes (String ruta){
		BufferedReader br;
		try {
			br = new BufferedReader (new FileReader (new File (ruta)));
			String lista = br.readLine();
			String actual = null;
			while (true){
				actual = br.readLine();
				if (actual == null)
					break;
				lista = lista + actual;	 
			}
			JSONArray array = new JSONArray(lista);
			for (Object obj : array)
			{
				JSONObject object =  (JSONObject) obj;
				int id = object.getInt("movieId");
				VOPelicula pelicula = tablaPeliculas.darValor(id);
				for (VOPelicula peliculaSimi : tablaPeliculas){
					try	{
						Double simi = object.getDouble(""+ peliculaSimi.getId());
						pelicula.agregarSimilitud(peliculaSimi.getId(), simi);
					}
					catch (JSONException e){
						assert (object.getString(""+ peliculaSimi.getId()).equals("NaN"));
					}
				}
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		calcularPrediccionesIniciales();
		/**for (VOUsuario user : tablaUsuarios){
			for (VOUsuarioPelicula userPeli : user.darUsuariosPelicula())
				System.out.println(userPeli.toString());
		}*/
		return true;
	}

	private void calcularPrediccionesIniciales2 (){
		for (VOUsuario usuario: tablaUsuarios){
			ListaEnlazada <Integer, VOUsuarioPelicula> listaUsuarioPelicula = usuario.darUsuariosPelicula().darLista();
			Nodo <Integer, VOUsuarioPelicula> actual = listaUsuarioPelicula.darPrimerNodo();
			while (actual != null){
				VOPelicula pelicula = tablaPeliculas.darValor(actual.key);
				double prediccion = calcularPrediccion(usuario, pelicula);
				actual.item.setRatingSistema(prediccion);
				assert (actual.item.getRatingSistema() == prediccion);
				//System.out.println(numUser + "\t" + actual.item.getRatingSistema() + "\t" + actual.item.getErrorRating() + "\t" + actual.item.getRatingUsuario());
				actual = actual.sig;
			}
		}
	}
	private void calcularPrediccionesIniciales (){
		for (VOUsuario usuario: tablaUsuarios){
			for (VOPelicula pelicula : tablaPeliculas){
				double prediccion = calcularPrediccion(usuario, pelicula);
				usuario.setRatingSistema(pelicula, prediccion);
				//System.out.println(numUser + "\t" + actual.item.getRatingSistema() + "\t" + actual.item.getErrorRating() + "\t" + actual.item.getRatingUsuario());
			}
		}
	}
	private double calcularPrediccion(VOUsuario usuario, VOPelicula pelicula) {
		double prediccion= 0.0, denominador =0.0;

		for (VORating rating : usuario.darRatings()){
			if(rating.getIdPelicula() != pelicula.getId()){
				double simi = pelicula.getSimilitud(rating.getIdPelicula());
				prediccion += rating.getRating() * simi;
				denominador+= simi;
			}
		}
		if (denominador != 0.0)
			return prediccion/denominador;
		else
			return 0.0;
	}
	@Override
	public int sizeMovies() {
		// TODO Auto-generated method stub
		return tablaPeliculas.darTamanio();
	}

	@Override
	public int sizeTeatros() {
		// TODO Auto-generated method stub
		return tablaTeatros.darTamanio();
	}

	public VOUsuario darUsuarioCualquiera(){
		VOUsuario usu =null;
		for (VOUsuario user: tablaUsuarios){
			usu = user;
			break;
		}
		return usu;
	}
	@Override
	public ILista<VOPeliculaPlan> PlanPeliculas(VOUsuario usuario, int fecha) {
		ListaEnlazadaSimple< VOPeliculaPlan> lista = new ListaEnlazadaSimple<VOPeliculaPlan>();
		VOUsuarioPelicula mejor = null; 
		VOTeatro teatroActual =null;
		long horaActual = 0;

		for (VOUsuarioPelicula userPeliculas : usuario.darUsuariosPelicula())
		{
			System.out.println(userPeliculas.getIdPelicula());

			mejor = userPeliculas;
			if (mejor.getRatingSistema() > 0)
			{
				VOPelicula mejorpelicula = tablaPeliculas.darValor(mejor.getIdPelicula());
				ListaEnlazadaSimple <VOPeliculaPlan> funcionSiguiente = mejorpelicula.darPeliculasSiguienteFuncion(fecha, horaActual);
				for(VOPeliculaPlan plan : funcionSiguiente)
				{
					if (lista.darNumeroElementos() == 0)
					{
						lista.agregarElementoFinal(plan);
						teatroActual = plan.getTeatro();
						horaActual = plan.getHoraFin().getTime();
						break;
					}
					if (plan.getTeatro().getNombre().equals(teatroActual))
					{
						lista.agregarElementoFinal(plan);
						teatroActual = plan.getTeatro();
						horaActual = plan.getHoraFin().getTime();
						break;
					}
					else if (plan.getHoraInicio().getTime() >= horaActual)
					{
						VOTeatro teatroSiguiente = plan.getTeatro();
						double tiempoRecorrido = grafo.darPesoArco(teatroActual, teatroSiguiente);

						if (plan.getHoraInicio().getTime() >= (horaActual+tiempoRecorrido))
						{
							lista.agregarElementoFinal(plan);
							teatroActual = plan.getTeatro();
							horaActual = plan.getHoraFin().getTime();
							break;
						}
					}
				}
			}
		}


		for (VOPeliculaPlan peli : lista)
		{
			System.out.println(peli.getPelicula().getTitulo());
		}

		return lista;
	}

	private VOPeliculaPlan buscarPlanRecomendado(VOUsuario usuario, VOTeatro llegada, int dia, Time tiempo) {
		VOPeliculaPlan recomendado = null;
		int numPelis = 0;
		for (VOPeliculaPlan peliPlan : llegada.getFunciones(tiempo, dia)){
			if (recomendado!= null && peliPlan.getHoraInicio().getTime() - tiempo.getTime() > 3600*1000)
				break;
			if (recomendado == null || usuario.compararPredicciones(recomendado.getPelicula(), peliPlan.getPelicula())<0)
				recomendado = peliPlan;
			numPelis++;
		}
		return recomendado;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorGenero(VOGeneroPelicula genero, VOUsuario usuario) {
		// TODO Auto-generated method stub
		
		ListaEnlazadaSimple< VOPeliculaPlan> listaRetorno = new ListaEnlazadaSimple<VOPeliculaPlan>();
		VOUsuarioPelicula mejor = null; 
		VOTeatro teatroActual =null;
		long horaActual = 0;

		for (int i = 1; i < 6; i++)
		{
			ListaEnlazadaSimple< VOPeliculaPlan> lista = new ListaEnlazadaSimple<VOPeliculaPlan>();
			for (VOUsuarioPelicula userPeliculas : usuario.darUsuariosPelicula())
			{
				System.out.println(userPeliculas.getIdPelicula());

				mejor = userPeliculas;
				if (mejor.getRatingSistema() > 0)
				{
					VOPelicula mejorpelicula = tablaPeliculas.darValor(mejor.getIdPelicula());
					ListaEnlazadaSimple <VOPeliculaPlan> funcionSiguiente = mejorpelicula.darPeliculasSiguienteFuncion(i, horaActual);
					for(VOPeliculaPlan plan : funcionSiguiente)
					{
						if (plan.getPelicula().tieneGenero(genero.getNombre()))
						{
							if (lista.darNumeroElementos() == 0)
							{
								lista.agregarElementoFinal(plan);
								teatroActual = plan.getTeatro();
								horaActual = plan.getHoraFin().getTime();
								break;
							}
							if (plan.getTeatro().getNombre().equals(teatroActual))
							{
								lista.agregarElementoFinal(plan);
								teatroActual = plan.getTeatro();
								horaActual = plan.getHoraFin().getTime();
								break;
							}
							else if (plan.getHoraInicio().getTime() >= horaActual)
							{
								VOTeatro teatroSiguiente = plan.getTeatro();
								double tiempoRecorrido = grafo.darPesoArco(teatroActual, teatroSiguiente);

								if (plan.getHoraInicio().getTime() >= (horaActual+tiempoRecorrido))
								{
									lista.agregarElementoFinal(plan);
									teatroActual = plan.getTeatro();
									horaActual = plan.getHoraFin().getTime();
									break;
								}
							}
						}
					}
				}
			}
			if (listaRetorno.darNumeroElementos() < lista.darNumeroElementos())
			{
				listaRetorno = lista;
			}

		}




		for (VOPeliculaPlan peli : listaRetorno)
		{
			System.out.println(peli.getPelicula().getTitulo()+ " // "+peli.getHoraInicio()+ " // "+ peli.getDia());
		}

		return listaRetorno;

	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorFranquicia(VOFranquicia franquicia, int fecha, String franja) {
		// TODO Auto-generated method stub
		
		
		return null;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorGeneroYDesplazamiento(VOGeneroPelicula genero, int fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorGeneroDesplazamientoYFranquicia(VOGeneroPelicula genero, int fecha,
			VOFranquicia franquicia) {
		return null;
	}

	@Override
	public ILista<IEdge<VOTeatro>> generarMapa() {
		ListaEnlazadaSimple<IEdge<VOTeatro>> lista = new ListaEnlazadaSimple<>();
		Iterable <Edge<VOTeatro>> iterable = grafo.prim();
		if (iterable == null)
			return null;
		for (Edge<VOTeatro> edge : iterable)
			lista.agregarElementoFinal(edge);
		return lista;
	}

	@Override
	public ILista<ILista<VOTeatro>> rutasPosible(VOTeatro origen, int n) {
		return grafo.rutas(origen, n);
	}

	public EncadenamientoSeparadoTH <Integer, VOUsuario> darUsuarios()
	{
		return tablaUsuarios;
	}


}
